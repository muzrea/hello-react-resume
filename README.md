# Setup

1.安装包依赖

```
yarn install
```

2.启动服务 localhost:1234，更新文件会实时加载

```
yarn start
```

3.启动Json Server http://localhost:3000
```
yarn server
```
需要请求数据

4.代码风格检测：

```
yarn lint
```
